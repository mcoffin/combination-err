# combination-err

Simple proc-macro crate for defining error enums

# DEPCRECATION NOTICE 20201008

As of 20201008, this crate has been deprecated in favor of the much better implementation found in [`thiserror`](https://crates.io/crates/thiserror), which does everything `combination-err` did, but better, and is already past the 1.0 mark.
