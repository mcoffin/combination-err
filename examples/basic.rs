extern crate combination_err;
use combination_err::combination_err;

use std::io;

#[combination_err("Basic error", "IO Error")]
#[derive(Debug)]
pub enum BasicError {
    Io(io::Error),
}

fn main() {
    let err = BasicError::Io(io::Error::new(io::ErrorKind::Other, "Some other error"));
    panic!("{}", err);
}
