#![recursion_limit="128"]

extern crate proc_macro;
extern crate proc_macro2;
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;
use quote::quote;
use syn::parse::{ Parse, ParseStream };
use syn::punctuated::Punctuated;
use syn::{parse_macro_input, Token};
use syn::spanned::Spanned;

struct CombinationErrorDescriptions {
    descriptions: Punctuated<syn::LitStr, Token![,]>,
}

impl CombinationErrorDescriptions {
    fn variants_len(&self) -> usize {
        match self.descriptions.len() {
            0 => 0,
            l => l - 1,
        }
    }
}

impl Parse for CombinationErrorDescriptions {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        Ok(CombinationErrorDescriptions {
            descriptions: input.parse_terminated(<syn::LitStr as Parse>::parse)?,
        })
    }
}

#[proc_macro_attribute]
pub fn combination_err(attribute: TokenStream, item: TokenStream) -> TokenStream {
    let descriptions = parse_macro_input!(attribute as CombinationErrorDescriptions);
    let enum_input = parse_macro_input!(item as syn::ItemEnum);

    if descriptions.variants_len() != enum_input.variants.len() {
        let s = format!("Number of descriptions ({}) does not match number of enum variants ({})", descriptions.variants_len(), enum_input.variants.len());
        panic!(syn::Error::new(descriptions.descriptions.span(), s));
    }

    let enum_ident = &enum_input.ident;
    let enum_generics = &enum_input.generics;
    let enum_description = {
        descriptions.descriptions.iter().next().unwrap()
    };
    let source_arms = enum_input.variants.iter()
        .map(|variant| {
            let variant_ident = &variant.ident;
            quote! {
                &#enum_ident::#variant_ident(ref e) => Some(e as &(dyn std::error::Error + 'static)),
            }
        });
    let source_description_arms = descriptions.descriptions.iter()
        .skip(1)
        .zip(enum_input.variants.iter())
        .map(|(description, variant)| {
            let variant_ident = &variant.ident;
            quote! {
                &#enum_ident::#variant_ident(..) => #description,
            }
        });
    let source_description = quote! {
        match self {
            #(#source_description_arms)*
        }
    };

    let from_implementations = enum_input.variants.iter()
        .map(|variant| {
            let variant_ty = variant.fields.iter()
                .next()
                .map(|field| field.ty.clone())
                .unwrap();
            let variant_ident = &variant.ident;
            quote! {
                impl#enum_generics From<#variant_ty> for #enum_ident#enum_generics {
                    fn from(e: #variant_ty) -> Self {
                        #enum_ident::#variant_ident(e)
                    }
                }
            }
        });
    TokenStream::from(quote! {
        #enum_input

        impl#enum_generics std::error::Error for #enum_ident#enum_generics {
            fn description(&self) -> &str {
                #enum_description
            }

            fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
                match self {
                    #(#source_arms)*
                }
            }
        }

        impl#enum_generics std::fmt::Display for #enum_ident#enum_generics {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                use std::error::Error;
                let description: &str = #enum_description;
                write!(f, "{}", description)?;
                if let Some(src) = self.source() {
                    let src_desc = #source_description;
                    write!(f, ": {}: {}", src_desc, src)?;
                }
                Ok(())
            }
        }

        #(#from_implementations)*
    })
}
